curdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
prefix=$(hostname | awk -F "-" '{print $5}')
pip3 install -r ${curdir}/requirements
s3path="s3://tms-prod-dispatchland-${prefix}-webfiles"
localpath="/var/www/${prefix}.dispatchland.com/api/public"
declare -a folders=("files" "images" "media")
#cd ${curdir}
mkdir ${curdir}/logs
for folder in "${folders[@]}"
do
  #echo ${folder}
  nohup ${curdir}/s3sync --config ${curdir}/s3sync.yml -v DEBUG push --s3path ${s3path}/${folder} --localpath ${localpath}/${folder} > ${curdir}/logs/sync_log_${folder}.txt &
done

